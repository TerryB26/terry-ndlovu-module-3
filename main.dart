// ignore_for_file: deprecated_member_use, prefer_const_constructors

import 'package:flutter/material.dart';

void main() 
{
  runApp(const MaterialApp
    (
      home: RegScreen()));
}

class RegScreen extends StatelessWidget 
{
  const RegScreen({Key? key}) : super(key: key);

@override
  Widget build(BuildContext context) {
    return Scaffold
      (
        backgroundColor: Colors.grey[900],        
        appBar: AppBar
        (
          title: const Text('Registration',style: TextStyle(color: Colors.white38),), 
          centerTitle: true,
          backgroundColor: Colors.grey[850],
          titleTextStyle: const TextStyle
          (
            fontSize: 20
          ),
          //backgroundColor: Colors.purple.shade900,
          
        ),
        body: Padding
        (
          padding: const EdgeInsets.fromLTRB(20, 200, 20, 200),

          


          child: Column
          (
            children:   <Widget> 
            [

            ///////////////////////////////////////////////////
            

           


            const TextField
            (
              decoration: InputDecoration
              (
                border: OutlineInputBorder
                (
                  borderSide: BorderSide(color: Colors.pink,width: 30),
                  
                  borderRadius: BorderRadius.all
                  (
                    Radius.circular(30.0),
                  )
                ),
                labelText: "Username",
                labelStyle: TextStyle(color: Colors.white),
                fillColor: Colors.cyan,
                filled: true,
                prefixIcon: Icon(Icons.account_circle)
              ),
            ),
            
              const SizedBox
              (  
                height: 50,
              ),
             ///////////////////////////////////////////////////
            
            const TextField
            (
              decoration: InputDecoration
              (
                border: OutlineInputBorder
                (

                  borderSide: BorderSide(color: Colors.pink,width: 30),
                  
                  borderRadius: BorderRadius.all
                  (
                    Radius.circular(30.0),
                  )
            
                ),
                labelText: "Email Adress",
                labelStyle: TextStyle(color: Colors.white),
                fillColor: Colors.cyan,
                filled: true,
                prefixIcon: Icon(Icons.mail_outline)
                
              ),
            ),
            
             ///////////////////////////////////////////////////
             const SizedBox
              (  
                height: 50,
              ),
            
              ///////////////////////////////////////////////////
             
            const TextField
            (
              decoration: InputDecoration
              (
                border: OutlineInputBorder
                (

                  borderSide: BorderSide(color: Colors.pink,width: 30),
                  
                  borderRadius: BorderRadius.all
                  (
                    Radius.circular(30.0),
                  )
            
                ),
                labelText: "Password",
                labelStyle: TextStyle(color: Colors.white),
                fillColor: Colors.cyan,
                filled: true,
                prefixIcon: Icon(Icons.key)
              ),
              
              obscureText: true,
            
            ),

           const SizedBox
              (  
                height: 100,
              ),

             RaisedButton
             (
              onPressed: () 
              {
                Navigator.of(context).push
                (
                  MaterialPageRoute(builder: (context) => const Screen2()
                  )
                );
              
              },
               child: const Text('Register',style: TextStyle(color: Colors.white),),
              color: Colors.cyan,
            ),

            ],
          ),
        ),
      );
    
  }
}





class Screen2 extends StatelessWidget 
{
  const Screen2({Key? key}) : super(key: key);

@override
  Widget build(BuildContext context) {
    return MaterialApp
    (
      home: Scaffold
      (
        backgroundColor: Colors.grey[900],        
        appBar: AppBar
        (
          title: const Text('Sign in',style: TextStyle(color: Colors.white38),), 
          centerTitle: true,
          backgroundColor: Colors.grey[850],
          titleTextStyle: const TextStyle
          (
            fontSize: 20
          ),
          //backgroundColor: Colors.purple.shade900,
          
        ),
        body: Padding
        (
          padding: const EdgeInsets.fromLTRB(20, 200, 20, 200),

          


          child: Column
          (
            children:   <Widget> 
            [

            ///////////////////////////////////////////////////
            

           


              const SizedBox
              (  
                height: 50,
              ),
             ///////////////////////////////////////////////////
            
            const TextField
            (
              decoration: InputDecoration
              (
                border: OutlineInputBorder
                (

                  borderSide: BorderSide(color: Colors.pink,width: 30),
                  
                  borderRadius: BorderRadius.all
                  (
                    Radius.circular(30.0),
                  )
            
                ),
                labelText: "Username or Email Adress",
                labelStyle: TextStyle(color: Colors.white),
                fillColor: Colors.cyan,
                filled: true,
                prefixIcon: Icon(Icons.mail_outline)
                
              ),
            ),
            
             ///////////////////////////////////////////////////
             const SizedBox
              (  
                height: 50,
              ),
            
              ///////////////////////////////////////////////////
             
            const TextField
            (
              decoration: InputDecoration
              (
                border: OutlineInputBorder
                (

                  borderSide: BorderSide(color: Colors.pink,width: 30),
                  
                  borderRadius: BorderRadius.all
                  (
                    Radius.circular(30.0),
                  )
            
                ),
                labelText: "Password",
                labelStyle: TextStyle(color: Colors.white),
                fillColor: Colors.cyan,
                filled: true,
                prefixIcon: Icon(Icons.key)
              ),
              
              obscureText: true,
            
            ),

           const SizedBox
              (  
                height: 100,
              ),

             RaisedButton
             (
            onPressed: () 
                {
                  Navigator.of(context).push
                  (
                    MaterialPageRoute(builder: (context) => const ScreenDash()
                    )
                  );
                },
                child: const Text('Sign in',style: TextStyle(color: Colors.white),),
                color: Colors.cyan,
              ),

            ],
          ),
        ),
      ),
    );
  }
}








class ScreenDash extends StatelessWidget 
{
  const ScreenDash({Key? key}) : super(key: key);

@override
  Widget build(BuildContext context) {
    return MaterialApp
    (
      home: Scaffold
      (
        backgroundColor: Colors.grey[900],        
        appBar: AppBar
        (
          title: const Text('Dashboard',style: TextStyle(color: Colors.white38),), 
          centerTitle: true,
          backgroundColor: Colors.grey[850],
          titleTextStyle: const TextStyle
         
          (
            fontSize: 20
          ),
          //backgroundColor: Colors.purple.shade900,
          
        ),
        
        body: 
         Center
        (



          child: 

          Material
          (
            
            color: Colors.grey[900],
            elevation: 10, 
            shadowColor: Colors.grey[850],
            
            child: InkWell 
          (
            
            onTap: () 
            {
              Navigator.of(context).push
                  (
                    MaterialPageRoute(builder: (context) => const AlertSec()
                    )
                  );
            },
            splashColor: Colors.grey[850],
            child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Ink.image
              (
                image: NetworkImage('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBYWFRgWFRUZGRgYGRocGBwcHB4cHhocGRgZHBocGhocIS4lIR4rIRoeJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMBgYGEAYGEDEdFh0xMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMf/AABEIAJ8BPgMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAABAgADBAUGB//EADkQAAEDAgQEBAUDAwQCAwAAAAEAAhEhMQMSQVEEYXGBIpGhsQUywdHwE0LhFFJiFSNy8VOCM2Oi/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/APnDMYN+VoHN3iP0HpPNJib79661/LpFq4DDa8lr3ZWxMyBXM0T4qUBJjWIQZQFdg4ZdRulz+ey2s+HMgZsZgJc4ES35QYafmkTU1FuYgzicJoY/LiMhkZWgtOeXAHL4pJE1JGh0qQy8Q8QGNsPwzzJ9gsybRLCCKSpCdmE4guDSQIkjSbeyBE7GzXqhamus6GbQnYZ7053oekoEe6TS1h0CVM5hFwlQRRGFAEDYd52E99PVIrGGTGhH8hVoK8S6AshFUzTCAhqgMdECVGtQNFEzh4WjkT5uP0QY2YG5jzRxXAk7Cg6CgQIFHtk8tOikobBAsbJmtWrgf06jEmDEFsyBD5LdJnJekA6wt+FwmAQXAYpbLgCBmmGzMBsggzelPIOO4VULF138HhNNRjDV1JAq0HxZYpJqJtFJCy8Z+hk8Dnl8to4aBpzHuYKDE150cR3IVuLiEQL0k99OwVQPdPjGs2zCfUoKyZSwExKICAM3T/qBIafl0Ht8tEFsqKtjourMyCKxmoG1OdRKrzc0mGSSBPtbVBaHEfUFOHgWzDo7+FQ55JJsD+d0ofsPOqC0LqYHENY1gdhMc5oJcSACJMguMSToAnfwmK0Va1gBMkhjQIE1qSNpOsBYeO4Z7IDxcurIMlsB2s0nXdBpZ8Vh0nDaRmmDFsoGWYi7WOoKFlLof6m2QTgMJrMhpmbT4akCnvJgjnN2QQWPfmdMASBQAASGgEwKCTXuq1EcyALVwvGFkQBQmJExmABjrlF9lmyqaIOi34xiDWRoHS6PDloSZt5yZlZuL41+JlzftBE7zcnsB5LMogseZAPY/T85KTSv4eX1QZY86edfoplp3QIAiCi1pMAAknQXPJRrTMQZ1EVEXogjDAJ5QOpP2lI51YRxXWaKnluaADn91SOqDfw2I5ws0xSvOI9iex5Ji1wFm/JJP/Gp+h81z2lMGoN7WEmQ1v7Ryo0kinX2VHEvMAFoAPiEbGn0SYIBmQZNoBMUd/Ctdhsr81PKZjayCjC1M/td6iFQt4w2SS2bgHo6Z9NEgw2f563pFKG3RBQyoUDTr7j7pp0HdKXboGgLRh8a9jQ0OgTNmmtDqNCARsarMFCEGzE+JYjgQXkhwLTRpoRX9qxlnfZE6IsZSTAFb0H8oAxkn3OwSPfM+Q5BXHEaJAkg0nft9Sqw4aU/N0FbU0gJ6bCvVAkbD1+6BQQVAJgTXRMHgaCPzW6nh5jpH4UFTlYwqZBv6fZRrQD0QR9kcChLtAPU2H5shejbnT6J8X5ssUb26nugrdGlY60FtVWrHupERY/ZICg3uxsQVzvpqHnpMg7Kl+I53zOJvck3vfdKx0GU5w5+WvLUdtUFYUKYUKiBVFJUQREmR0QUH0PsgiikKIHB8Pf3B+yGnc/REfKeo9AfukBQOzELXBwuCCJ3BkLs8DxDnNaGvYwjw5XAicpww00dJJmLAUdoSuLfr+WSoPSOxnggNxsACLmwOUg2dOkC9uQXB4zin4gaXtADZiGx8xkz3UDjkoSIMX3qqg8zMmd0FY36+acPI1Vn6nhg1FaUEc7XWt3wbF/YA8bggViSK7EETy3oAwtn5i+BWNT2+6d/GHSazJMa8gFq4j4Xiy2QA1xygyIEhx/bJ/aTSdEP9Fxjm8LfAQPmFZnflBrHzBBkZxTpku594I16qOxyRE3gWijZj3Q4nh3sIa9sGAex6d/JVH87oHBulL+SRtbV2Vxwt3V6UHdBTKCuGDu4dpPmU2HgG/hIA3pOk+/ZArcMwNBJqeyXFfJiwFvv3ut3+kYrgx4yvD5iHAEQJqHRFK00BUZ8IeQPDNQKOZdwBAMupII0/cN0GFrgoJNAFYGt0BPU/ZBxkRYbBArmf5N8/sE+DhszeNwyw75TWcpyxT+6EvDNOagBje21VqLP/rGgBzA0bSLVNL90FzsPhTmh7xqCZkVgNAitwe1xVYeJw2hxLCXYYiCb2F6CKzoFosSThC5rIoIkyQJ3qqOIaW5SAG0ihvEybC6CgOsnlQ4m7Wk9PsQjnAgtbXmZjp/KAtw4mtagAaTQ1U/UcKA0ExQGndEGnVBBGvLvmqK1IqNu0pXYRbfW0CaLo8Mzhy1mdzg6uaJr4zQ+E/ti3OYgTa3D4aBmxHC9gTWa3btFYFtLIOWoooEFofNHa67d0jhBr+Qr+A4Q4j8oIFJr2+62O+FvjxOZ4Wh0yaCJM0tz5bIOUot7/hTwJOWha011c/IBaDBvEwo/4W8MLyW5QJETWsDT396IMBKjSoi1pNhKAGi6X9DhEw3HaKGZsCCNTEyDQDUGtFgLDNjppyR/SO0daBB0GfD2ECcdg2qB80EE5iKb86UglZeK4ZjMsPDpE0gxyMEwqMRwJpYCB2Wng/hz8QSzLXNQmD4ck0/9xXkgzFsVoRan5RKQumz4PiFjvlo4akzcGIGlaXpzE58fgnsZncBGYNFQalpcLbiD5IM4bLY1BJ9PehVaswnV6089UpjZABY/moVjeJeP3mNiTFgPYAdAqyVo+Gg/qsgTlcHxWow/G4UBNmmwKCt2ISMwcREGATFP3NrdZ3Yjv7jHU7k+5J7r0uIyc88MMwDg5zntrIvmN6NJmSb1koYzQTlbw1SzEY3KW/M7NNIpl8Vv7oMmEHmXOLjUkm2pPIKz9D+4xyFT30TtIA8IidZkpUHSxPijXODnYLCQSdJIqMpOWrRNuQ70cRxbXNIGExsxBaBSDO19O5maRkQKAqIBwRQWNxn0Ae6ny+I06VomdxD5nO7l4jtFK7UVIRcgbMDeh3j3hK9sXQRL4G42KCqb1/lMcQ2zHzKkAgkCIjWboA9EE/UMfMaGlSlqeaBB0XYx8PhYluI8EFxAINRTKJy01qd0HKDYqbIH/oclu4/DwafpuLrVIImlaECxH/65LH+mgYWH53UTPuUqCJ2PFiJH1SKIGkf9fZRrUHCqAQQ8102cdg+GeHaYa1prEw0gmmpJv9gudE1HdKg6X9Tga4ImTJk0k+HrA3us3E5C4FgawRUSTWTJkjoOyzuQaEFroGknWtErnm1hsEhKgQWMeamTAHqaD85KslO+gjuev8fdUufsgdBKx6sagsxG2FoA/n19lXIkbfRWYwmXAyPvWqqAQdN/9MS4gYgFMokf5SDM7NrzNrIYn9PB/wDkzdomKig3+iwMHiA5j3Qcbz1QPxIZnOQksplzXsJnvKTDxC0y0wRPqCDfcEjulUQbG8diEOOc/KB18ZdWb+JxJ5lJh/EMRsQ91CTfV0knuTPVJhWPMEeQJ+ypQGyBCZyVACqy9O58JWQSAaAkSe9SgDDqmGIrmYDD++J6U68qHbRV4mEAJDwaxHnzt90BBRcqA06BXhtB0QBBFRABTmNRuFcOCeQC0FwIBEQaEkVAtUEdQqiFfw/FvYIa8igFhYEu23cT3QL/AEr6wx8ikZXX2NPRUDcrbifEMUEtzmKiDBES6kERHiNOfRY0Gx/w3FFDhupsMw6S2RPJVO4V4GYsdlFZikEwDNokivNP/XYn977k3NzM+cqO4x7gQ55LTEjeDIHmgzhyhGyhCLBJjf8AJQKEcy38Jg4LmAveWvBM6yKQAOmvPWsF3CcOL8RBnVpNNoHvPZBz3GUAjHMKxvDvLcwaS2YkVrExTkgqTtE3tqfZFuA82afKNJ15VVr8NwplPh5diT3p2QTCeGPzENfGbwkSCS0gSDsTPZahxuEInAFq+KppFJFBdc40/K+SqeCg6n9bgnL/ALAgNIPiNSTMjaJNK6bKrjOLY4+DDDCSSSCTfbbyoua0wiTQHqgbEmyZ+A7+09hPsg4GLG9PzunGM6suqTMXM9esUQT9NwHymen517qB/T/tA8Q/c1vzUCBmOj6haXcE+A5rS4OAIjxXj+3nRZCu0zimZIGM5pIaD4AB8jAYhtYyxpIaBShQc7h+FeTRjzI8PhNc1AQdr15FZwu2zjMNhgYzgLfJUZRQVbag8/8AGvEdiM0BjefWCEAAUThkiWyR6jqArcDAlwDoAJrLg2nMmw5xRBtwOEwixs4oYSP3EESR4qDpAEz5rBxLQ1xDXZhStK05fm9V0H8FhuJJxQ1os05Zyy4CSDE0/aDQt3WbiuEYz5cVjzygCs2Mnb1CDGUFeMIkae8+UqstI2I6z/0gpxAlBWhmGXGGtLjsBJpU0CUYNjBrahqaH6jzCCoO0StadrflV0+HwsJzRneWmkjLIBL3g1/4hh7neljuGwJEYxAJqMpkSQCCd6zNqHuHPcUAFvwsPAzeN5yjPYOkw85CPCQJb6qri2YYALHkyYg6ANBJOvzGBuGk8gGUFQhF7j35rsv4PBqWjEAcAWeFxMGCCaGh70dYXIcUlRdM8LhVyuxCZbHhNWl4Do8M/LUb5heqXFwsEMJzPLyKBwIAJqK5RNO2vJBhxNDuB6U+iRWPApXQfVLGqAAKQVJUlAQOYQJ2HQ/dSFNPzmgAedeyuZxEAUnus+YV6IGoE6U/PVA4C08Nxb2fKQKzYGsRKzymZvFBX7eqDoP+L4k3AOYuADRDSTMSa3qlHxnEbkqIY6QIob0PKKfkrnzr+c1Q4oLcV5cSZrUk76k9dUrXEqsFX4eHW4rbz1QBjeVUzmCkjtp6aog6eZQmqBcVkX1tqgwbKwwdEAIQBBPTzUa2SEFn6Yb8zuw/Lqt/EAUbIHqe/wBFfw2Jhlzxig+JwhwJ8Ikh1BeBUUuALFWOfwxiWYgMVAIiYgVPn15UQcx7pQRndOxlybD1JsPT0QMHeA/8h7FM02IVTnzSABMwP5VgKCx1BW7vb+VWAnx/mPVIgIcRYqxuLSonTnX/AKVQCIKC/Axwx2Zsgibw64ixHNbB8UxSR/u2iMwH+N5/4i3PdczKpEIGxnEuJNyST1JkpFa12ahvofYHkqkEUa6p5R6z9lFAgi1s+I4oAAeYADRQUAGUC1oWRFqDa74pi2z0iIytgzEyI5KnH4x725XOkB2aIAqZkyBzPms6LRUTaaoGfp09yT7FImeamd0qAtUhAKIJKIRRFKoFLYEJsIAAk6mnaZSuFd02KbDYR3ufUoEW/h8bDDIeyTOlJMnUQbGNQMoMFYFYRQDqT9PQeqDY7i8AE/7BginiNDvfpruufxT2OeSxmVtIaTMUrXqkxHSkaUAVzLKtrarpcOcEhoeHNP7nNmsuNTM2GWwH7r0QYxr+ahALpsdw1RDzzMSBWRSmvo3mlb/TQR/uEkCtAJpypbnc8igwtMTGyQqzEyh5yEloJyk3I0J63ha2DAyDPnD40sfE+14MZeUgWqUGBFzy0AgXmvTQbLohnDiCS4th85paZzNykBu7cxi1QCdUnEjhjmDXvkNJYSIDnR8pEUaIvrJ5IOVCaDExTfT8qrMPAzAkOAiLrTh4bm/uZIEa1F48ygyYVSJAify53QxHkyKQDoAN/wCVtzv8MFlQYvYAVOs1UxcNzhGZvzcwdh7nsAg5ysY5DFZlMTNBPetE3D1cBuUF2P8AMeqrATF8kzqfdCyAEot3QRKCONlJUKCAwnzg/NffXvuqwVIQEsIJGyAVr6tB2ofofp5KpAyA1UAULkACMIpUD4m+49bFIn/b0PoY+3qq0BhRRQhBEdPzmgtXBlkEPaYkHMJkNyvkXirsmh1QV4RuSJivfT19lXTn7ro4ZwGtkh9ZoYh0OOW0Gwr1vvh4kNzH9MuLaRN7CZjnKAMANTppv+FI59TN9U+Fe0giDGyrxMCDSo0QVgSVaMCL+pA90zGBlf3eyn6cVJjakk80FuBDXEvEjKYoHAkjw6imtDpzWriMbhiHZMN4cWuiSIDjamY+ELE2LSCNqjva6nh696ed0HQxMThi0kYbmzJFaGbTDpyhK93DAjwYmWRqDIgTHioZneg3MjAXA3FRsURikUgR0+6CPYJMQASS0E1A0k7wp+nEF1APadOd0WOJMHxct+myq4l4JgWFKGnNBXivkylJpHn9lM3IfnNPhjUoI0QDP4VezEaYbkzUpEaSTFPRVPbMJGkjlH1H8DzQawwf+MX/ALhJ0P18uSq4jFEZQyDA7b/RVDGcLOMit5rfohIcBJgik6HadkCA8lfhMiXbUHUg3R4XiDhud4QZaWGdJLSSCLO8NDpMro4fxJmUn9BkuOwiggwMtBb13oHMRAlaOJxw8N8DWETJaIzTFSAKW9Ss+ZBJ2QRLUEBagoi77eyAKBRWARU1JsPqUAY6DJtY9FHtg77HdaMDhHYgljTM5aAmzS6u1Atbfgz4gxEiKwQ4gSBuKgSN+qDkuRynZbuM4R+GJcGgTHhrWJv/AD6VWIPd/cfMoCMPcx5z5BPlZvJ0AkDzVRPdAFB3+G4rExGEhjJY3IJaTAAvY1E6AmmlJuxcF4IP6OEAGAkQbZHFzaA3LgP/AFEH5o81KgQdT4rxJzOY/DYHZw9xbN3MFuoIJ5rn+H/LpT3n6KshRBZ+pFgB6nzKn6zv7iq0Q03gxX0ifceYQM80aeX1KbhwCagmml9PRLEt5t9j/PunxHZQG9CSKSSJ8gD7oNXCcSwBwewPLgIsMpDXiRNKuLJHI6gLRjcRgeIjDLczfDBNHZQKjPFCJ7rkotqCO/lQ+/ogsbiCw8Pr5m6qc03NecylTNcRYoFTEev4FpwuDc8Aht4iog5nECmlQUuNwjwCS3rUcjv/AJBBnlFKAiUD4epGggdTT7qr9NWH5R386JTv5oEOH2TlAqyQBWZNo05oK3lVMdUVWiG6knlF/VGQakVrUUpbugyK1jOScBo0nmfoAna8kw0RNOfmgjsHxZnWNYNzyjqlc8nX6I4hFI0pO+v1SIDdAhQIgoJqoQnOGbxzmmhAPqUGs8iD6GJ8wgRFyOIzKYcIP5snZRpdE1A5SQSJGtigVjdTb35BK50mUXkms/wlhBAU8eHvPaw/OajGVtOw8/smIiQKms8gKmPJBZw2K0Pa57QYNQbOgRUFa/6rCBLhgNq6YzAgCCIAigmDtpsuUV1G/F/mzYbXAumJNKQBUG1/sgz8XxDHjwYeRxdMzIiIIAjU15aLIuhi8az/AMLKyZgTUyYkG1QM2bS8VxYtXEwACSQBYAmw5IEJAv5I0VeNp0TtsgObZHr+dkHGFP0CY8QkgGK/uEjSEEotnDfEXMAAaKAgSXauLpgOiZJrFqWmaWsLILpz1gSIAIgEkXuaJP1Z+YE85g9zBkdUFvDgAZyKAxG9KyNbgd/N8LAY4ScUMcXGjqyAGnNI3LvQqo4n7oAaLAAai1BW2qoCD//Z'),
                height: 200,
                width: 200,
                fit: BoxFit.cover,
                ),

              SizedBox(height: 0),
              Text('View Alerts',style: TextStyle(color: Colors.white38,fontSize: 20),), 
              SizedBox(height: 0),
              

              
            ],


          ),

          ),


          ),

          // Image.network
          // (
            
          //  ),
          
        ),



      floatingActionButton: FloatingActionButton.extended
      (
        extendedPadding: const EdgeInsets.symmetric(vertical: 8,horizontal: 30),
      onPressed: () 
              {
                Navigator.of(context).push
                  (
                    MaterialPageRoute(builder: (context) => const SettingsScr()
                    )
                  );
              },
                icon: const Icon(Icons.add),
                label: const Text('Settings'),
                backgroundColor: const Color.fromARGB(255, 71, 14, 14),

      ),

        
       ),
      );
  }
}



class AlertSec extends StatelessWidget 
{
  const AlertSec({Key? key}) : super(key: key);

@override
  Widget build(BuildContext context) {
    return Scaffold
      (
        backgroundColor: Colors.grey[900],        
        appBar: AppBar
        (
          title: const Text('Alerts',style: TextStyle(color: Colors.white38),), 
          centerTitle: true,
          backgroundColor: Colors.grey[850],
          titleTextStyle: const TextStyle
          (
            fontSize: 20
          ),
          //backgroundColor: Colors.purple.shade900,
          
        ),
        body: Center
        (
          child: Text('No Available alerts in the moment',style: TextStyle(color: Colors.white38)),
          
        ),


      );
    
  }
}






class SettingsScr extends StatelessWidget 
{
  const SettingsScr({Key? key}) : super(key: key);

@override
  Widget build(BuildContext context) {
    return Scaffold
      (
        backgroundColor: Colors.grey[900],        
        appBar: AppBar
        (
          title: const Text('Edit Profile',style: TextStyle(color: Colors.white38),), 
          centerTitle: true,
          backgroundColor: Colors.grey[850],
          titleTextStyle: const TextStyle
          (
            fontSize: 20
          ),
          //backgroundColor: Colors.purple.shade900,
          
        ),
        body: Padding
        (
          padding: const EdgeInsets.fromLTRB(20, 200, 20, 200),

          


          child: Column
          (
            children:   <Widget> 
            [

            ///////////////////////////////////////////////////
            

           


              const SizedBox
              (  
                height: 50,
              ),
             ///////////////////////////////////////////////////
            
            const TextField
            (
              decoration: InputDecoration
              (
                border: OutlineInputBorder
                (

                  borderSide: BorderSide(color: Colors.pink,width: 30),
                  
                  borderRadius: BorderRadius.all
                  (
                    Radius.circular(30.0),
                  )
            
                ),
                labelText: "Username",
                labelStyle: TextStyle(color: Colors.white),
                fillColor: Colors.cyan,
                filled: true,
                prefixIcon: Icon(Icons.mail_outline)
                
              ),
            ),
            
             ///////////////////////////////////////////////////
             const SizedBox
              (  
                height: 50,
              ),
            
              ///////////////////////////////////////////////////
             
            const TextField
            (
              decoration: InputDecoration
              (
                border: OutlineInputBorder
                (

                  borderSide: BorderSide(color: Colors.pink,width: 30),
                  
                  borderRadius: BorderRadius.all
                  (
                    Radius.circular(30.0),
                  )
            
                ),
                labelText: "Password",
                labelStyle: TextStyle(color: Colors.white),
                fillColor: Colors.cyan,
                filled: true,
                prefixIcon: Icon(Icons.key)
              ),
              
              obscureText: true,
            
            ),

            const SizedBox
              (  
                height: 50,
              ),
                  const TextField
            (
              decoration: InputDecoration
              (
                border: OutlineInputBorder
                (

                  borderSide: BorderSide(color: Colors.pink,width: 30),
                  
                  borderRadius: BorderRadius.all
                  (
                    Radius.circular(30.0),
                  )
            
                ),
                labelText: "Confirm Password",
                labelStyle: TextStyle(color: Colors.white),
                fillColor: Colors.cyan,
                filled: true,
                prefixIcon: Icon(Icons.key)
              ),
              
              obscureText: true,
            
            ),
           const SizedBox
              (  
                height: 100,
              ),

             RaisedButton
             (
            onPressed: () 
                {
                  Navigator.of(context).push
                  (
                    MaterialPageRoute(builder: (context) => const DetailChange()
                    )
                  );
                },
                child: const Text('Confirm Changes',style: TextStyle(color: Colors.white),),
                color: Colors.cyan,
              ),

            ],
          ),
        ),


      );
    
  }
}





class DetailChange extends StatelessWidget 
{
  const DetailChange({Key? key}) : super(key: key);

@override
  Widget build(BuildContext context) {
    return Scaffold
      (
        backgroundColor: Colors.grey[900],        
        appBar: AppBar
        (
          title: const Text('Alerts',style: TextStyle(color: Colors.white38),), 
          centerTitle: true,
          backgroundColor: Colors.grey[850],
          titleTextStyle: const TextStyle
          (
            fontSize: 20
          ),
          //backgroundColor: Colors.purple.shade900,
          
        ),
        body: Center
        (
          child: Text('Profile Successfully updated',style: TextStyle(color: Colors.white38)),
          
          
        ),

        floatingActionButton: FloatingActionButton.extended
              (
                extendedPadding: const EdgeInsets.symmetric(vertical: 8,horizontal: 30),
              onPressed: () 
                      {
                        Navigator.of(context).push
                          (
                            MaterialPageRoute(builder: (context) => const ScreenDash()
                            )
                          );
                      },
                        //icon: const Icon( Icons.g ),
                        label: const Text('Dashboard'),
                        backgroundColor: const Color.fromARGB(255, 71, 14, 14),

              ),

      );
    
  }
}

